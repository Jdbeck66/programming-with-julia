{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "blond-manchester",
   "metadata": {},
   "source": [
    "# Visualizing Tabular Data\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "basic-devices",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "## ❓Overview\n",
    "**Questions**\n",
    "- How can I visualize tabular data in Julia?\n",
    "- How can I group several plots together?\n",
    "\n",
    "**Objectives**\n",
    "- Plot simple graphs from data.\n",
    "- Group several graphs in a single figure."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "blank-madagascar",
   "metadata": {},
   "source": [
    "## Visualizing Data\n",
    "\n",
    "The mathematician Richard Hamming once said, “The purpose of computing is insight, not numbers,” and the best way to develop insight is often to visualize data. Visualization deserves an entire lecture of its own, but we can explore a few features of Julia’s implementation of the matplotlib library here. While there is no official plotting library, matplotlib is the de facto standard. First, we will import the Plots package and use two of its functions to create and display a heat map of our data:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "given-spain",
   "metadata": {},
   "source": [
    "[Plots.jl Documentation](https://docs.juliaplots.org/latest/)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "filled-unknown",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Only Once\n",
    "import Pkg\n",
    "Pkg.add(\"Plots\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "simplified-taxation",
   "metadata": {},
   "outputs": [],
   "source": [
    "using Plots, CSV, DataFrames, Statistics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abroad-column",
   "metadata": {},
   "outputs": [],
   "source": [
    "apoedata = DataFrame(CSV.File(\"../Data/apoe_fold_change.csv\"));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "prescribed-attachment",
   "metadata": {},
   "outputs": [],
   "source": [
    "E₃ = apoedata.ApoE3;\n",
    "E₄ = apoedata.ApoE4\n",
    "Genes = apoedata.Gene;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "reliable-universe",
   "metadata": {},
   "outputs": [],
   "source": [
    "pyplot() #matplotlib implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "genetic-swaziland",
   "metadata": {},
   "outputs": [],
   "source": [
    "histogram(E₄)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "boxed-granny",
   "metadata": {},
   "outputs": [],
   "source": [
    "histogram(E₃, lab = \"E₃\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "thorough-coach",
   "metadata": {},
   "outputs": [],
   "source": [
    "scatter(E₄, E₃, lab = \"Genes\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "overall-massachusetts",
   "metadata": {},
   "outputs": [],
   "source": [
    "heatmap(Genes[1:5], Genes[1:5],(E₄[1:5].-transpose(E₃[1:5])), c = :blues, xlab=\"ApoE₄\", ylab=\"ApoE₃\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "parallel-emerald",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot((sort(E₄).- mean(E₄))./std(E₄), lab = \"E₄\")\n",
    "plot!((sort(E₃).- mean(E₃))./std(E₃), lab = \"E₃\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "classified-london",
   "metadata": {},
   "source": [
    "[Plots Attributes](https://docs.juliaplots.org/latest/attributes/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ahead-tonight",
   "metadata": {},
   "source": [
    "## Grouping Plots\n",
    "---\n",
    "You can group similar plots in a single figure using subplots. Use the layout keyword, and optionally the convenient @layout macro to generate arbitrarily complex subplot layouts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "pursuant-rider",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(pl₁, pl₂, pl₃,pl₄, layout = 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recent-savings",
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(pl₁, pl₂, pl₃, layout = (3,1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "broadband-badge",
   "metadata": {},
   "outputs": [],
   "source": [
    "l = @layout [a ; b c]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recent-cream",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig₁ = plot(pl₁, pl₂, pl₃, layout = l)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sapphire-toner",
   "metadata": {},
   "source": [
    "The call to `savefig` stores the plot as a graphics file. This can be a convenient way to store your plots for use in other documents, web pages etc. The graphics format is automatically determined by `Plots` from the file name ending we specify; below PNG from ‘fig1.png’. `Plots` supports many different graphics formats, including SVG and PDF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "matched-event",
   "metadata": {},
   "outputs": [],
   "source": [
    "savefig(fig₁,\"fig1.png\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "promotional-powell",
   "metadata": {},
   "source": [
    "### Play Around\n",
    "---\n",
    "You can find some other general examples [here](http://docs.juliaplots.org/latest/tutorial/) and [here](http://docs.juliaplots.org/latest/generated/pyplot/). Additionally, you can adjust various ploat attributes using the parameters found [here](http://docs.juliaplots.org/latest/attributes/)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "entitled-strategy",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-danger\">\n",
    "\n",
    "### 📌 Key Points\n",
    "---\n",
    "- Use the Plots package for creating simple visualizations."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
