{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "optimum-receiver",
   "metadata": {},
   "source": [
    "# Analyzing Protein Data\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "covered-guide",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "    \n",
    "## ❓Overview\n",
    "\n",
    "**Questions**\n",
    "- How can I process tabular data files in Julia?\n",
    "\n",
    "**Objectives**\n",
    "- Explain what a library is and what libraries are used for.\n",
    "- Import a Julia package and use the functions it contains.\n",
    "- Read tabular data from a file into a program.\n",
    "- Select individual values and subsections from data.\n",
    "- Perform operations on arrays of data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bulgarian-calendar",
   "metadata": {},
   "source": [
    "Words are useful, but what’s more useful are the sentences and stories we build with them. Similarly, while a lot of powerful, general tools are built into Julia, specialized tools built up from these basic units live in libraries that can be called upon when needed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "structural-wednesday",
   "metadata": {},
   "source": [
    "## Loading data into Julia\n",
    "---\n",
    "\n",
    "To begin processing our data, we need to first load it into Julia. To accomplish this, we're going to use the `CSV` and `DataFrames` packages, which will help us load our file of `comma-separated-values` into a convenient structure for analays. We need to first install each package and then call the packages to be used. The installation will only need to be done once on our computer and, thereafter, they will be available for our use. \n",
    "\n",
    "To install the `CSV` and `DataFrames` packages, enter:\n",
    "\n",
    "```julia\n",
    "using Pkg\n",
    "Pkg.add(\"CSV\")\n",
    "Pkg.add(\"DataFrames\")\n",
    "```\n",
    "\n",
    "This may take awhile to complete."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "continuing-fancy",
   "metadata": {},
   "outputs": [],
   "source": [
    "# ONLY DO ONCE\n",
    "using Pkg\n",
    "Pkg.add(\"CSV\")\n",
    "Pkg.add(\"DataFrames\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "completed-novel",
   "metadata": {},
   "source": [
    "To use the `CSV` Package, enter:\n",
    "\n",
    "```julia\n",
    "using CSV, DataFrames\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "adjacent-forum",
   "metadata": {},
   "outputs": [],
   "source": [
    "using CSV, DataFrames"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "upper-today",
   "metadata": {},
   "source": [
    "Importing a package is like getting a piece of lab equipment out of a storage locker and setting it up on the bench. Packages provide additional functionality to the basic Julia package, much like a new piece of equipment adds functionality to a lab space.\n",
    "\n",
    "Now that we've imported our packages, we'll read in data into a variable called `apoedata`, giving the location of our data file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "resident-coordinator",
   "metadata": {},
   "outputs": [],
   "source": [
    "apoedata = CSV.File(\"../Data/apoe_fold_change.csv\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "corrected-hughes",
   "metadata": {},
   "source": [
    "The expression `CSV.File(...)` is a function call that asks Julia to run the function `File` which belongs to the `CSV` package. \n",
    "\n",
    "As an example, John Smith is the person named John that belongs to the Smith family. We could use the notation above to write his name smith.john, just as `File` is a function that belongs to the `CSV` package.\n",
    "\n",
    "`CSV.File` has a single parameter in our example above: the name of the file we want to read. This needs to be a character string, so we put it in quotes.`CSV.File` will take other parameters if our data file requires special handling. For example, if instead of comma's separating our values our file used semi-colons, we could write: `CSV.File(\"../Data/apoe_fold_change.csv\", delim=';')`.\n",
    "\n",
    "Lets put our data into a structure called a dataframe. We can so so by passing our `apoedata` variable to the `DataFrame` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "irish-fault",
   "metadata": {},
   "outputs": [],
   "source": [
    "dfₐ = DataFrame(apoedata)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "heated-dylan",
   "metadata": {},
   "source": [
    "Notice that not all of our rows are displayed (e.g., our display stops at row 30). If we want to show all 49 of our rows, we could use the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "indirect-rescue",
   "metadata": {},
   "outputs": [],
   "source": [
    "show(dfₐ, allrows=true)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "behavioral-maintenance",
   "metadata": {},
   "source": [
    "We can also convert our dataframe into another data type called a Matrix (or a 2 dimensional array)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "viral-kernel",
   "metadata": {},
   "outputs": [],
   "source": [
    "dfₘ = convert(Matrix, dfₐ)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "amber-checkout",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "\n",
    "## Data Type\n",
    "---\n",
    "We can learn about our various data structures by using the `typeof` command, as in the following examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "packed-composition",
   "metadata": {},
   "outputs": [],
   "source": [
    "typeof(dfₐ)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "together-hydrogen",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Determine the type of `apoedata` below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sonic-management",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "sticky-independence",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Determine the type of dfₘ below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "agricultural-equation",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "seeing-series",
   "metadata": {},
   "source": [
    "We can also learn about the size of our data structure by invoking the `size` command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "electronic-speaking",
   "metadata": {},
   "outputs": [],
   "source": [
    "size(dfₐ)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "indonesian-separation",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Determine the size of the `apoedata' below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "intensive-russell",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "controlled-former",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Determine the size of dfₘ below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cathedral-moderator",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "homeless-retailer",
   "metadata": {},
   "source": [
    "The output tells us that the data array variable contains 49 rows and 3 columns. When we created the variable `dfₘ` to store our data, we did not only create the array; we also created information about the array, called members or attributes. This extra information describes data in the same way an adjective describes a noun. `size` is an attribute of `dfₘ` which describes its dimensions. \n",
    "\n",
    "## Accessing Values\n",
    "---\n",
    "If we want to get a single number from the array, we must provide an index in square brackets after the variable name, just as we do in math when referring to an element of a matrix. Our inflammation data has two dimensions, so we will need to use two indices to refer to one specific value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "secret-lighting",
   "metadata": {},
   "outputs": [],
   "source": [
    "apoedata[1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "freelance-template",
   "metadata": {},
   "outputs": [],
   "source": [
    "apoedata.ApoE3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "confirmed-layer",
   "metadata": {},
   "outputs": [],
   "source": [
    "dfₘ[1:3,1:3]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "medieval-novelty",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Find the value in dfₐ at row 20, column 1 below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acquired-wildlife",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "medium-hopkins",
   "metadata": {},
   "source": [
    "The expression `dfₐ[20, 1]` accesses the element at row 20, column 1. Programming languages like Julia, Fortran, MATLAB and R start counting at 1 because that’s what human beings have done for thousands of years. Languages in the C family (including C++, Java, Perl, and Python) count from 0 because it represents an offset from the first value in the array (the second value is offset by one index from the first value). This is closer to the way that computers represent arrays (if you are interested in the historical reasons behind counting indices from zero, you can read [Mike Hoye’s blog post](http://exple.tive.org/blarg/2013/10/22/citation-needed/). As a result, if we have an M×N array in Julia, its indices go from 1 to M on rows and 1 to N on columns. \n",
    "\n",
    "![matrix calculation](https://i.imgur.com/s3maTll.png)\n",
    "\n",
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "### In the Corner\n",
    "---\n",
    "What may also surprise you is that when Julia displays an array, it shows the element with index [1, 1] in the upper left corner rather than the lower left. This is consistent with the way mathematicians draw matrices but different from the Cartesian coordinates. The indices are (row, column) instead of (column, row) for the same reason, which can be confusing when plotting data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "saved-oliver",
   "metadata": {},
   "source": [
    "## Analyzing Data\n",
    "---\n",
    "\n",
    "Working with a the data frame, we're going to obtain summary statistics, sort and filter our data, and perform some basic calculations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "southwest-cooper",
   "metadata": {},
   "outputs": [],
   "source": [
    "describe(dfₐ)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "capital-prince",
   "metadata": {},
   "outputs": [],
   "source": [
    "sort(dfₐ,:ApoE3, rev = true)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "checked-graphic",
   "metadata": {},
   "outputs": [],
   "source": [
    "filter(row -> row.ApoE3 > 100, dfₐ)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "accomplished-therapist",
   "metadata": {},
   "outputs": [],
   "source": [
    "Diffₐ = (dfₐ.ApoE4 - dfₐ.ApoE3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "lasting-nature",
   "metadata": {},
   "outputs": [],
   "source": [
    "insertcols!(dfₐ, 4, :difference => Diffₐ)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "modular-banner",
   "metadata": {},
   "outputs": [],
   "source": [
    "using Statistics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "younger-artwork",
   "metadata": {},
   "outputs": [],
   "source": [
    "dfₐ"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "unlimited-closer",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "mean(dfₐ.ApoE3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "committed-drawing",
   "metadata": {},
   "outputs": [],
   "source": [
    "select!(dfₐ, Not(:difference))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "unlimited-biodiversity",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Find the mean of the fold change difference between ApoE₄ and ApoE₃ below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "prescription-touch",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "collected-cannon",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Find the maximum of the fold change difference between ApoE₄ and ApoE₃ below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fossil-arabic",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "accomplished-parish",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Find the minimum of the fold change difference between ApoE₄ and ApoE₃ below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "extreme-value",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "steady-hierarchy",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Find the median of the fold change difference between ApoE₄ and ApoE₃ below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "surrounded-faith",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "charitable-reception",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Find the standard deviaton (std) of the fold change difference between ApoE₄ and ApoE₃ below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "joint-settlement",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "decimal-routine",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Assign the maximum, minimum and standard deviation of the differences above to three appropriately named variables below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "simplified-partnership",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "accomplished-jumping",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Print the contents of these variables so that another person could understand them below*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "christian-heater",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "remarkable-kuwait",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean(dfₐ[1,2:3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "boring-hundred",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean(dfₐ[:,2])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dynamic-prototype",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "### 📌 Not All Functions Have Input\n",
    "---\n",
    "Generally, a function uses inputs to produce outputs. However, some functions produce outputs without needing any input. For example, checking the current time doesn’t require any input.\n",
    "\n",
    "To install the `Dates` package, enter:\n",
    "\n",
    "```julia\n",
    "using Pkg\n",
    "Pkg.add(\"Dates\")\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "inner-surface",
   "metadata": {},
   "outputs": [],
   "source": [
    "# ONLY DO ONCE\n",
    "using Pkg\n",
    "Pkg.add(\"Dates\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "soviet-interest",
   "metadata": {},
   "outputs": [],
   "source": [
    "using Dates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "conservative-avenue",
   "metadata": {},
   "outputs": [],
   "source": [
    "now()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "legendary-fountain",
   "metadata": {},
   "outputs": [],
   "source": [
    "Dates.format(now(), \"HH:MM\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "going-defensive",
   "metadata": {},
   "source": [
    "### Slicing Strings\n",
    "---\n",
    "A section of an array is called a slice. We can take slices of character strings as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ceramic-generation",
   "metadata": {},
   "outputs": [],
   "source": [
    "element = \"oxygen\"\n",
    "println(\"first three characters: \", element[1:3])\n",
    "println(\"last three characters: \", element[4:6])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dynamic-china",
   "metadata": {},
   "outputs": [],
   "source": [
    "element[begin:end]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "beginning-carnival",
   "metadata": {},
   "source": [
    "### Stacking Arrays\n",
    "---\n",
    "Arrays can be concatenated and stacked on top of one another, using Julia’s vcat and hcat functions for vertical and horizontal stacking, respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "amber-calibration",
   "metadata": {},
   "outputs": [],
   "source": [
    "A = [[1, 2, 3] [4, 5, 6] [7, 8, 9]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "inappropriate-bible",
   "metadata": {},
   "outputs": [],
   "source": [
    "typeof(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "assigned-freedom",
   "metadata": {},
   "outputs": [],
   "source": [
    "B = hcat(A, A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recorded-absorption",
   "metadata": {},
   "outputs": [],
   "source": [
    "C = vcat(A,A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "danish-activity",
   "metadata": {},
   "outputs": [],
   "source": [
    "D = [A A]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "geological-killer",
   "metadata": {},
   "outputs": [],
   "source": [
    "E = [A;A]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "relevant-integer",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-danger\">\n",
    "\n",
    "\n",
    "### 📌 Key Points\n",
    "---\n",
    "- Import a package into a program using `Pkg.add(\"packagename\")` and `using packagename`.\n",
    "- Use the `CSV` and `DataFrames` to work with arrays in Julia.\n",
    "- The expression `size()` gives the dimensions of an array.\n",
    "- Use array`[x, y]` to select a single element from a 2D array.\n",
    "- Use `low:high` to specify a slice that includes the indices from low to high.\n",
    "- Use `# some kind of explanation` to add comments to programs.\n",
    "- Use the `Statistics` package to implement mean(array), maximum(array), and minimum(array) to calculate simple statistics."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.6.0",
   "language": "julia",
   "name": "julia-1.6"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
