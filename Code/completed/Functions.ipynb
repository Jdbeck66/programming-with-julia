{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ready-battlefield",
   "metadata": {},
   "source": [
    "# Creating Functions\n",
    "---\n",
    "\n",
    "## Overview\n",
    "---\n",
    "**Questions**\n",
    "\n",
    "- How can I define new functions?\n",
    "- What’s the difference between defining and calling a function?\n",
    "- What happens when I call a function?\n",
    "\n",
    "**Objectives**\n",
    "\n",
    "- Define a function that takes parameters.\n",
    "- Return a value from a function.\n",
    "- Test and debug a function.\n",
    "- Set default values for function parameters.\n",
    "- Explain why we should divide programs into small, single-purpose functions.\n",
    "\n",
    "\n",
    "At this point, we’ve written code to draw some plots in our ApoE Fold changed data, looped over data, and manipulate data. But, what if we had thousands of datasets, and didn’t want to generate a plot for every single one? Also, what if we want to reuse our code, on a different dataset or at a different point in our program? Cutting and pasting it is going to make our code get very long and very repetitive, very quickly. We’d like a way to package our code so that it is easier to reuse, and Julia provides for this by letting us define things called ‘functions’ — a shorthand way of re-executing longer pieces of code. Let’s start by defining a function fahr_to_celsius that converts temperatures from Fahrenheit to Celsius:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "respiratory-blogger",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "fahr_to_celsius (generic function with 1 method)"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function fahr_to_celsius(temp)\n",
    "    return ((temp-32)*(5/9))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "specialized-transsexual",
   "metadata": {},
   "source": [
    "The function definition opens with the keyword `function` followed by the name of the function (fahr_to_celsius) and a parenthesized list of parameter names (temp). The body of the function — the statements that are executed when it runs — is indented below the definition line. The body concludes with a `return` keyword followed by the return value. The function concludes with the keyword `end`,\n",
    "\n",
    "When we call the function, the values we pass to it are assigned to those variables so that we can use them inside the function. Inside the function, we use a return statement to send a result back to whoever asked for it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "micro-papua",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.0"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fahr_to_celsius(32)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "formed-jerusalem",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "freezing point of water: 0.0C\n",
      "boiling point of water: 100.0C\n"
     ]
    }
   ],
   "source": [
    "println(\"freezing point of water: \", fahr_to_celsius(32), 'C')\n",
    "println(\"boiling point of water: \", fahr_to_celsius(212), 'C')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sustained-progress",
   "metadata": {},
   "source": [
    "### Composing Functions\n",
    "---\n",
    "Now that we’ve seen how to turn Fahrenheit into Celsius, we can also write the function to turn Celsius into Kelvin:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "removable-disclosure",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "freezing point of water in Kelvin: 273.15\n"
     ]
    }
   ],
   "source": [
    "function celsius_to_kelvin(temp_c)\n",
    "    return temp_c + 273.15\n",
    "end\n",
    "println(\"freezing point of water in Kelvin: \", celsius_to_kelvin(0.))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "parental-council",
   "metadata": {},
   "source": [
    "What about converting Fahrenheit to Kelvin? We could write out the formula, but we don’t need to. Instead, we can compose the two functions we have already created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "dental-research",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "boiling point of water in Kelvin: 373.15"
     ]
    }
   ],
   "source": [
    "function fahr_to_kelvin(temp_f)\n",
    "    temp_c = fahr_to_celsius(temp_f)\n",
    "    temp_k = celsius_to_kelvin(temp_c)\n",
    "    return temp_k\n",
    "end\n",
    "\n",
    "print(\"boiling point of water in Kelvin: \", fahr_to_kelvin(212.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "manufactured-rebel",
   "metadata": {},
   "source": [
    "This is our first taste of how larger programs are built: we define basic operations, then combine them in ever-larger chunks to get the effect we want. Real-life functions will usually be larger than the ones shown here — typically half a dozen to a few dozen lines — but they shouldn’t ever be much longer than that, or the next person who reads it won’t be able to understand what’s going on.\n",
    "\n",
    "### Testing and Documenting\n",
    "---\n",
    "Once we start putting things in functions so that we can re-use them, we need to start testing that those functions are working correctly. To see how to do this, let’s write a function to offset a dataset so that it’s mean value shifts to a user-defined value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "paperback-florida",
   "metadata": {},
   "outputs": [],
   "source": [
    "using Statistics, CSV, DataFrames"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "valid-corner",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "offset_mean (generic function with 1 method)"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "function offset_mean(data, target_mean_value)\n",
    "    return (data .- mean(data)) .+ target_mean_value\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "id": "immune-validation",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2×2 Array{Float64,2}:\n",
       " 0.0  0.0\n",
       " 0.0  0.0"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "z = zeros((2,2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "id": "thorough-landing",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[3.0 3.0; 3.0 3.0]"
     ]
    }
   ],
   "source": [
    "print(offset_mean(z,3.0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "electoral-trance",
   "metadata": {},
   "outputs": [],
   "source": [
    "dfₐ = DataFrame(CSV.File(\"../Data/apoe_fold_change.csv\"));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "id": "intelligent-threat",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "49×49 Array{Float64,2}:\n",
       "     1.58397e5       1.53229e5  …  18469.3    18362.8    18260.4\n",
       "     2.93019e5       2.83459e5     34166.6    33969.5    33780.0\n",
       "     1.9966e5        1.93146e5     23280.7    23146.4    23017.3\n",
       "     2.09599e5       2.02761e5     24439.7    24298.7    24163.2\n",
       " 65769.9         63624.2            7668.9     7624.67    7582.13\n",
       "  3834.99         3709.87       …    447.167    444.588    442.108\n",
       " 38038.9         36797.9            4435.41    4409.83    4385.22\n",
       "     1.36217e5  131773.0           15883.2    15791.6    15703.5\n",
       " 59113.2         57184.6            6892.71    6852.96    6814.72\n",
       " 21602.3         20897.5            2518.87    2504.34    2490.37\n",
       " 61728.9         59714.9        …   7197.71    7156.19    7116.26\n",
       " 48709.8         47120.6            5679.66    5646.9     5615.39\n",
       "     1.28426e5       1.24236e5     14974.7    14888.3    14805.3\n",
       "     ⋮                          ⋱                        \n",
       " 13996.1         13539.5            1631.97    1622.56    1613.51\n",
       "  6139.35         5939.05            715.86     711.731    707.76\n",
       "  2834.92         2742.43            330.557    328.65     326.816\n",
       " 77253.6         74733.2        …   9007.93    8955.97    8906.0\n",
       " 26784.0         25910.2            3123.07    3105.06    3087.73\n",
       "  2305.76         2230.53            268.856    267.305    265.814\n",
       " 28373.7         27448.0            3308.43    3289.35    3270.99\n",
       "  9786.99         9467.69           1141.18    1134.6     1128.27\n",
       " 31226.3         30207.5        …   3641.04    3620.04    3599.84\n",
       " 46896.0         45366.0            5468.17    5436.63    5406.3\n",
       " 31190.0         30172.4            3636.81    3615.84    3595.66\n",
       " 43907.4         42474.9            5119.68    5090.16    5061.75"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "data = dfₐ.ApoE3 * transpose(dfₐ.ApoE4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "coordinate-dover",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "49×49 Array{Float64,2}:\n",
       "      1.39505e5       1.34338e5  …    -421.868    -528.392    -630.857\n",
       "      2.74128e5       2.64568e5      15275.4     15078.3     14888.8\n",
       "      1.80768e5       1.74254e5       4389.49     4255.21     4126.06\n",
       "      1.90708e5  183870.0             5548.49     5407.53     5271.94\n",
       "  46878.7         44732.9           -11222.3    -11266.5    -11309.1\n",
       " -15056.2        -15181.3        …  -18444.0    -18446.6    -18449.1\n",
       "  19147.7         17906.7           -14455.8    -14481.4    -14506.0\n",
       "      1.17326e5       1.12882e5      -3008.03    -3099.64    -3187.76\n",
       "  40221.9         38293.4           -11998.5    -12038.3    -12076.5\n",
       "   2711.07         2006.29          -16372.3    -16386.9    -16400.8\n",
       "  42837.6         40823.7        …  -11693.5    -11735.0    -11775.0\n",
       "  29818.6         28229.4           -13211.6    -13244.3    -13275.8\n",
       "      1.09535e5       1.05345e5      -3916.51    -4002.87    -4085.95\n",
       "      ⋮                          ⋱                          \n",
       "  -4895.12        -5351.74          -17259.2    -17268.7    -17277.7\n",
       " -12751.9        -12952.2           -18175.4    -18179.5    -18183.5\n",
       " -16056.3        -16148.8           -18560.7    -18562.6    -18564.4\n",
       "  58362.4         55842.0        …   -9883.29    -9935.24    -9985.22\n",
       "   7892.8          7018.97          -15768.1    -15786.2    -15803.5\n",
       " -16585.5        -16660.7           -18622.4    -18623.9    -18625.4\n",
       "   9482.48         8556.78          -15582.8    -15601.9    -15620.2\n",
       "  -9104.22        -9423.53          -17750.0    -17756.6    -17762.9\n",
       "  12335.0         11316.3        …  -15250.2    -15271.2    -15291.4\n",
       "  28004.8         26474.8           -13423.0    -13454.6    -13484.9\n",
       "  12298.8         11281.2           -15254.4    -15275.4    -15295.6\n",
       "  25016.1         23583.7           -13771.5    -13801.1    -13829.5"
      ]
     },
     "execution_count": 36,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "offₛ = offset_mean(data,0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "precise-courtesy",
   "metadata": {},
   "source": [
    "It’s hard to tell from the default output whether the result is correct, but there are a few tests that we can run to reassure us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "id": "broad-infrastructure",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "original min, mean, and max are:265.8138132753488\t18891.216988663582\t293018.867571242\n",
      "min, mean, and max of offset data are: -18625.403175388234\t0.0\t274127.6505825784\n"
     ]
    }
   ],
   "source": [
    "println(\"original min, mean, and max are:\", \n",
    "    minimum(data), \"\\t\",\n",
    "    mean(data), \"\\t\",\n",
    "    maximum(data))\n",
    "println(\"min, mean, and max of offset data are: \",\n",
    "    minimum(offₛ), \"\\t\",\n",
    "    mean(offₛ), \"\\t\",\n",
    "    maximum(offₛ))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "alleged-microwave",
   "metadata": {},
   "source": [
    "That seems almost right: the original mean was about 18891.2, so the lower bound from zero is now about ~ -18600. We can even go further and check that the standard deviation hasn’t changed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "id": "boring-encoding",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "std dev before and after: 26409.118464908683\t26409.118464908683"
     ]
    }
   ],
   "source": [
    "print(\"std dev before and after: \", std(data),\"\\t\" ,std(offₛ))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "talented-error",
   "metadata": {},
   "source": [
    "Those values look the same, but we probably wouldn’t notice if they were different in the sixth decimal place. Let’s do this instead:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "id": "copyrighted-outline",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "difference in standard deviations before and after: 0.0"
     ]
    }
   ],
   "source": [
    "print(\"difference in standard deviations before and after: \",\n",
    "      std(data) - std(offₛ))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "central-owner",
   "metadata": {},
   "source": [
    "Again, the difference is very small. It’s still possible that our function is wrong, but it seems unlikely enough that we should probably get back to doing our analysis. We have one more task first, though: we should write some documentation for our function to remind ourselves later what it’s for and how to use it.\n",
    "\n",
    "The usual way to put documentation in software is to add comments like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "id": "retained-accordance",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "offset_mean (generic function with 1 method)"
      ]
     },
     "execution_count": 42,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# offset_mean(data, target_mean_value):\n",
    "# return a new array containing the original data with its mean offset to match the desired value.\n",
    "function offset_mean(data, target_mean_value)\n",
    "    return (data - mean(data)) + target_mean_value\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "surrounded-cologne",
   "metadata": {},
   "source": [
    "There’s a better way, though. If the first thing in a function is a string that isn’t assigned to a variable, that string is attached to the function as its documentation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "id": "published-inspection",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "offset_mean"
      ]
     },
     "execution_count": 48,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"\"\"    \n",
    "    Return a new array containing the original data\n",
    "       with its mean offset to match the desired value.\n",
    "\n",
    "    Examples\n",
    "    --------\n",
    "    >>> offset_mean([1, 2, 3], 0)\n",
    "    array([-1.,  0.,  1.])\n",
    "\n",
    "\"\"\"\n",
    "function offset_mean(data, target_mean_value)\n",
    "    return (data - numpy.mean(data)) + target_mean_value \n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "id": "mounted-helen",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "search: \u001b[0m\u001b[1mo\u001b[22m\u001b[0m\u001b[1mf\u001b[22m\u001b[0m\u001b[1mf\u001b[22m\u001b[0m\u001b[1ms\u001b[22m\u001b[0m\u001b[1me\u001b[22m\u001b[0m\u001b[1mt\u001b[22m\u001b[0m\u001b[1m_\u001b[22m\u001b[0m\u001b[1mm\u001b[22m\u001b[0m\u001b[1me\u001b[22m\u001b[0m\u001b[1ma\u001b[22m\u001b[0m\u001b[1mn\u001b[22m\n",
      "\n"
     ]
    },
    {
     "data": {
      "text/latex": [
       "\\begin{verbatim}\n",
       "Return a new array containing the original data\n",
       "   with its mean offset to match the desired value.\n",
       "\n",
       "Examples\n",
       "--------\n",
       ">>> offset_mean([1, 2, 3], 0)\n",
       "array([-1.,  0.,  1.])\n",
       "\\end{verbatim}\n"
      ],
      "text/markdown": [
       "```\n",
       "Return a new array containing the original data\n",
       "   with its mean offset to match the desired value.\n",
       "\n",
       "Examples\n",
       "--------\n",
       ">>> offset_mean([1, 2, 3], 0)\n",
       "array([-1.,  0.,  1.])\n",
       "```\n"
      ],
      "text/plain": [
       "\u001b[36m  Return a new array containing the original data\u001b[39m\n",
       "\u001b[36m     with its mean offset to match the desired value.\u001b[39m\n",
       "\u001b[36m  \u001b[39m\n",
       "\u001b[36m  Examples\u001b[39m\n",
       "\u001b[36m  --------\u001b[39m\n",
       "\u001b[36m  >>> offset_mean([1, 2, 3], 0)\u001b[39m\n",
       "\u001b[36m  array([-1.,  0.,  1.])\u001b[39m"
      ]
     },
     "execution_count": 49,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "?offset_mean"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "valuable-elizabeth",
   "metadata": {},
   "source": [
    "### Defining Defaults\n",
    "---\n",
    "The idea here is that there are default parameters that we may want to change. But if we choose not to make those changes, then the defaults will be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "id": "legal-parade",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "no parameters: \n",
      "a: 1\tb: 2\tc: 3\n",
      "one parameter: \n",
      "a: 55\tb: 2\tc: 3\n",
      "two parameters: \n",
      "a: 55\tb: 66\tc: 3"
     ]
    }
   ],
   "source": [
    "function display(a=1, b=2, c=3)\n",
    "    print(\"a: \", a,\"\\t\", \"b: \", b,\"\\t\", \"c: \", c)\n",
    "end\n",
    "\n",
    "println(\"no parameters: \")\n",
    "display()\n",
    "println()\n",
    "println(\"one parameter: \")\n",
    "display(55)\n",
    "println()\n",
    "println(\"two parameters: \")\n",
    "display(55, 66)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "third-alliance",
   "metadata": {},
   "source": [
    "### Anonymous Functions\n",
    "---\n",
    "An anonymous function is created without being given a name. As an example:\n",
    "\n",
    "```julia\n",
    "x -> x^2 + 2x -1\n",
    "```\n",
    "\n",
    "These type of functions are primarily passed to functions that take other functions as arguments, such as below, where we want to `map` a function onto an array of input values, returning values transformed by the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "stainless-horror",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3-element Array{Int64,1}:\n",
       "  2\n",
       " 14\n",
       " -2"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    " map(x -> x^2 + 2x - 1, [1, 3, -1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fabulous-tracker",
   "metadata": {},
   "source": [
    "You can apply this to multiple inputs by using:\n",
    "\n",
    "```julia\n",
    "(x,y,z) -> x^2 + 2y - z\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "charged-check",
   "metadata": {},
   "source": [
    "### 📌 Key Points\n",
    "---\n",
    "- Define a function using `function function_name(parameter)`.\n",
    "- Call a function using function_name(value).\n",
    "- Variables defined within a function can only be seen and used within the body of the function.\n",
    "- If a variable is not defined within the function it is used, Julia looks for a definition before the function call\n",
    "- Use help(thing) to view help for something.\n",
    "- Put docstrings in functions to provide help for that function.\n",
    "- Specify default values for parameters when defining a function using name=value in the parameter list."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
