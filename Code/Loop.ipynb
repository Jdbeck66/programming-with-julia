{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "multiple-subscription",
   "metadata": {},
   "source": [
    "# Repeating Actions with Loops\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "shared-antarctica",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "## ❓**Questions**\n",
    "- How can I do the same operations on many different values?\n",
    "\n",
    "**Objectives**\n",
    "- Explain what a for loop does.\n",
    "- Correctly write for loops to repeat simple calculations.\n",
    "- Trace changes to a loop variable as the loop runs.\n",
    "- Trace changes to other variables as they are updated by a for loop."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "victorian-reaction",
   "metadata": {},
   "source": [
    "In the last session, we wrote Julia code to plot the data from our ApoE fold data. But what if we had many more data sets that we wanted to execute the same analysis upon. In this session, we will learn about how to teach the computer to repeat things using a simple set of examples. Let's start with printing out each individual letter in a string of letters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "duplicate-verse",
   "metadata": {},
   "outputs": [],
   "source": [
    "word = \"nucleotide\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "australian-consciousness",
   "metadata": {},
   "outputs": [],
   "source": [
    "typeof(word)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "victorian-seventh",
   "metadata": {},
   "source": [
    "In Julia, a string is basically an ordered collection of characters, and every character has a unique number associated with it – its index. This means that we can access characters in a string using their indices. For example, we can get the first character of the word 'nucleotide', by using word[1]. One way to print each character is to use ten print statements:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fourth-intervention",
   "metadata": {},
   "outputs": [],
   "source": [
    "println(word[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "entire-relaxation",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Extend the code above to print out the whole word variable below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "irish-techno",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "challenging-delicious",
   "metadata": {},
   "source": [
    "This is a bad approach for three reasons:\n",
    "\n",
    "1. Not scalable. Imagine you need to print characters of a string that is hundreds of letters long. It might be easier to type them in manually.\n",
    "\n",
    "2. Difficult to maintain. If we want to decorate each printed character with an asterisk or any other character, we would have to change four lines of code. While this might not be a problem for short strings, it would definitely be a problem for longer ones.\n",
    "\n",
    "3. Fragile. If we use it with a word that has more characters than what we initially envisioned, it will only display part of the word’s characters. A shorter string, on the other hand, will cause an error because it will be trying to display part of the string that doesn’t exist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "light-sullivan",
   "metadata": {},
   "outputs": [],
   "source": [
    "println(word[11])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sensitive-annotation",
   "metadata": {},
   "source": [
    "This is a better method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "early-holder",
   "metadata": {},
   "outputs": [],
   "source": [
    "for char in word\n",
    "    println(char)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "adjusted-velvet",
   "metadata": {},
   "outputs": [],
   "source": [
    "word = \"amino acid\"\n",
    "for char in word\n",
    "    println(char)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecological-copper",
   "metadata": {},
   "source": [
    "The improved version uses a for loop to repeat an operation — in this case, printing — once for each thing in a sequence. The general form of a loop is:\n",
    "\n",
    "```julia\n",
    "for variable in collection\n",
    "    # do some stuff\n",
    "end\n",
    "```\n",
    "\n",
    "![loop](https://i.imgur.com/hZ9k7I4.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "working-regular",
   "metadata": {},
   "source": [
    "[]()\n",
    "\n",
    "where each character (char) in the variable word is looped through and printed one character after another. The numbers in the diagram denote which loop cycle the character was printed in (1 being the first loop, and 10 being the final loop).\n",
    "\n",
    "And, we can call the loop variable anything we like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "minor-hygiene",
   "metadata": {},
   "outputs": [],
   "source": [
    "for banana in word\n",
    "    println(banana)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "worthy-electricity",
   "metadata": {},
   "source": [
    "It is a good idea to choose variable names that are meaningful, otherwise it would be more difficult to understand what the loop is doing.\n",
    "\n",
    "Here’s another loop that repeatedly updates a variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "experienced-disaster",
   "metadata": {},
   "outputs": [],
   "source": [
    "l = 0\n",
    "for vowel in \"aeiou\"\n",
    "    l = l + 1\n",
    "    #l += 1\n",
    "end\n",
    "print(\"There are \", l, \" vowels\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "olympic-foundation",
   "metadata": {},
   "source": [
    "It’s worth tracing the execution of this little program step by step. Since there are five characters in 'aeiou', the statement on line 3 will be executed five times. The first time around, length is zero (the value assigned to it on line 1) and vowel is 'a'. The statement adds 1 to the old value of length, producing 1, and updates length to refer to that new value. The next time around, vowel is 'e' and length is 1, so length is updated to be 2. After three more updates, length is 5; since there is nothing left in 'aeiou' for Julia to process, the loop finishes and the print statement on line 4 tells us our final answer.\n",
    "\n",
    "Note that a loop variable is a variable that’s being used to record progress in a loop. It still exists after the loop is over, and we can re-use variables previously defined as loop variables as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "supported-shoot",
   "metadata": {},
   "outputs": [],
   "source": [
    "letter = 'z'\n",
    "for letter in \"abc\"\n",
    "    println(letter)\n",
    "end\n",
    "print(\"after the loop, letter is \", letter)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "circular-accident",
   "metadata": {},
   "source": [
    "Note also that finding the length of a string is such a common operation that Python actually has a built-in function to do it called length:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "sharing-edwards",
   "metadata": {},
   "outputs": [],
   "source": [
    "length(word)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "apparent-arnold",
   "metadata": {},
   "outputs": [],
   "source": [
    "lastindex(word)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "least-soldier",
   "metadata": {},
   "source": [
    "### From 1 to N\n",
    "---\n",
    "Julia has a built-in function called `collect` that generates a sequence of numbers from a start number to an end number in steps of a given size. For example, to create a sequennce of numbers starting at 1 and ending at 13 in increments of 2, one would type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "renewable-configuration",
   "metadata": {},
   "outputs": [],
   "source": [
    "collect(1:2:13)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "stable-lying",
   "metadata": {},
   "source": [
    "Write a loop that prints the first three natural numbers using `collect`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "binding-marsh",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Write a loop that iterates from 1 to 20 in 2 step increments and prints each step on a separate line below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "radio-english",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "detected-fleet",
   "metadata": {},
   "source": [
    "### Computing Powers with Loops\n",
    "---\n",
    "To exponentiate in Julia:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "reduced-alliance",
   "metadata": {},
   "outputs": [],
   "source": [
    "5^3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cardiac-charles",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Write a loop using multiplication (without exponentiation) that calculates 5 to the 3rd power below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "lined-texas",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "fatal-missouri",
   "metadata": {},
   "source": [
    "Knowing that two strings can be concatenated using the `string(a, b) = \"ab\"` function, write a loop that takes a string and produces a new string with the characters in reverse order, so 'Newton' becomes 'notweN'."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cultural-andrew",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Knowing that two strings can be concatenated using the string(a, b) = \"ab\" function, write a loop that takes a string and produces a new string with the characters in reverse order, so **Newton** becomes **notweN** below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "occupied-insight",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "superb-dublin",
   "metadata": {},
   "source": [
    "### Computing the Value of a Polynomial\n",
    "---\n",
    "The built-in function enumerate takes a sequence (e.g. a list) and generates a new sequence of the same length. Each element of the new sequence is a pair composed of the index (0, 1, 2,…) and the value from the original sequence:\n",
    "\n",
    "```julia\n",
    "\n",
    "a = [\"a\", \"b\", \"c\"];\n",
    "\n",
    "for (index, value) in enumerate(a)\n",
    "     println(\"$index $value\")\n",
    "end\n",
    "\n",
    "```\n",
    "\n",
    "The code above loops through a_list, assigning the index to idx and the value to val.\n",
    "\n",
    "Suppose you have encoded a polynomial as a list of coefficients in the following way: the first element is the constant term, the second element is the coefficient of the linear term, the third is the coefficient of the quadratic term, etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "charitable-bankruptcy",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 5\n",
    "coefs = [2,4,3]\n",
    "y = coefs[1] * x^0 + coefs[2] * x^1 + coefs[3] * x^2\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rocky-capital",
   "metadata": {},
   "source": [
    "Write a loop using enumerate(coefs) which computes the value y of any polynomial, given x and coefs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "brave-point",
   "metadata": {},
   "outputs": [],
   "source": [
    "y = 0\n",
    "for (idx, coef) ∈ enumerate(coefs)\n",
    "    y = y + coef * x^(idx-1)\n",
    "end\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cathedral-appeal",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-danger\">\n",
    "    \n",
    "### 📌 Key Points\n",
    "---\n",
    "- Use for variable in sequence to process the elements of a sequence one at a time.\n",
    "- Use length(thing) to determine the length of something that contains other values."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
