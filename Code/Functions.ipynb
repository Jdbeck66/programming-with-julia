{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ready-battlefield",
   "metadata": {},
   "source": [
    "# Creating Functions\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fifty-currency",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "## ❓Overview\n",
    "---\n",
    "**Questions**\n",
    "\n",
    "- How can I define new functions?\n",
    "- What’s the difference between defining and calling a function?\n",
    "- What happens when I call a function?\n",
    "\n",
    "**Objectives**\n",
    "\n",
    "- Define a function that takes parameters.\n",
    "- Return a value from a function.\n",
    "- Test and debug a function.\n",
    "- Set default values for function parameters.\n",
    "- Explain why we should divide programs into small, single-purpose functions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acceptable-burning",
   "metadata": {},
   "source": [
    "At this point, we’ve written code to draw some plots in our ApoE Fold changed data, looped over data, and manipulate data. But, what if we had thousands of datasets, and didn’t want to generate a plot for every single one? Also, what if we want to reuse our code, on a different dataset or at a different point in our program? Cutting and pasting it is going to make our code get very long and very repetitive, very quickly. We’d like a way to package our code so that it is easier to reuse, and Julia provides for this by letting us define things called ‘functions’ — a shorthand way of re-executing longer pieces of code. Let’s start by defining a function fahr_to_celsius that converts temperatures from Fahrenheit to Celsius:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "respiratory-blogger",
   "metadata": {},
   "outputs": [],
   "source": [
    "function fahr_to_celsius(temp)\n",
    "    return ((temp-32)*(5/9))\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "specialized-transsexual",
   "metadata": {},
   "source": [
    "The function definition opens with the keyword `function` followed by the name of the function (fahr_to_celsius) and a parenthesized list of parameter names (temp). The body of the function — the statements that are executed when it runs — is indented below the definition line. The body concludes with a `return` keyword followed by the return value. The function concludes with the keyword `end`,\n",
    "\n",
    "When we call the function, the values we pass to it are assigned to those variables so that we can use them inside the function. Inside the function, we use a return statement to send a result back to whoever asked for it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "micro-papua",
   "metadata": {},
   "outputs": [],
   "source": [
    "fahr_to_celsius(70)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "incorporated-fossil",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Calculate the celsius value at 32 degrees fahrenheit below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "formed-jerusalem",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "alleged-pakistan",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Calculate the celsius value at 212 degrees fahrenheit below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "personal-ceremony",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "sustained-progress",
   "metadata": {},
   "source": [
    "### Composing Functions\n",
    "---\n",
    "Now that we’ve seen how to turn Fahrenheit into Celsius, we can also write the function to turn Celsius into Kelvin:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "removable-disclosure",
   "metadata": {},
   "outputs": [],
   "source": [
    "function celsius_to_kelvin(temp_c)\n",
    "    return temp_c + 273.15\n",
    "end\n",
    "println(\"freezing point of water in Kelvin: \", celsius_to_kelvin(0.))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "parental-council",
   "metadata": {},
   "source": [
    "What about converting Fahrenheit to Kelvin? We could write out the formula, but we don’t need to. Instead, we can compose the two functions we have already created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dental-research",
   "metadata": {},
   "outputs": [],
   "source": [
    "function fahr_to_kelvin(temp_f)\n",
    "    temp_c = fahr_to_celsius(temp_f)\n",
    "    temp_k = celsius_to_kelvin(temp_c)\n",
    "    return temp_k\n",
    "end\n",
    "\n",
    "print(\"boiling point of water in Kelvin: \", fahr_to_kelvin(212.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "manufactured-rebel",
   "metadata": {},
   "source": [
    "This is our first taste of how larger programs are built: we define basic operations, then combine them in ever-larger chunks to get the effect we want. Real-life functions will usually be larger than the ones shown here — typically half a dozen to a few dozen lines — but they shouldn’t ever be much longer than that, or the next person who reads it won’t be able to understand what’s going on.\n",
    "\n",
    "### Testing and Documenting\n",
    "---\n",
    "Once we start putting things in functions so that we can re-use them, we need to start testing that those functions are working correctly. To see how to do this, let’s write a function to offset a dataset so that it’s mean value shifts to a user-defined value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "paperback-florida",
   "metadata": {},
   "outputs": [],
   "source": [
    "using Statistics, CSV, DataFrames"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "valid-corner",
   "metadata": {},
   "outputs": [],
   "source": [
    "function offset_mean(data, target_mean_value)\n",
    "    return (data .- mean(data)) .+ target_mean_value\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "immune-validation",
   "metadata": {},
   "outputs": [],
   "source": [
    "z = zeros((2,2))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "thorough-landing",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(offset_mean(z,3.0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "electoral-trance",
   "metadata": {},
   "outputs": [],
   "source": [
    "dfₐ = DataFrame(CSV.File(\"../Data/apoe_fold_change.csv\"));"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "intelligent-threat",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = dfₐ.ApoE3 * transpose(dfₐ.ApoE4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "coordinate-dover",
   "metadata": {},
   "outputs": [],
   "source": [
    "offₛ = offset_mean(data,0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "precise-courtesy",
   "metadata": {},
   "source": [
    "It’s hard to tell from the default output whether the result is correct, but there are a few tests that we can run to reassure us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "broad-infrastructure",
   "metadata": {},
   "outputs": [],
   "source": [
    "println(\"original min, mean, and max are:\", \n",
    "    minimum(data), \"\\t\",\n",
    "    mean(data), \"\\t\",\n",
    "    maximum(data))\n",
    "println(\"min, mean, and max of offset data are: \",\n",
    "    minimum(offₛ), \"\\t\",\n",
    "    mean(offₛ), \"\\t\",\n",
    "    maximum(offₛ))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "alleged-microwave",
   "metadata": {},
   "source": [
    "That seems almost right: the original mean was about 18891.2, so the lower bound from zero is now about ~ -18600. We can even go further and check that the standard deviation hasn’t changed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "boring-encoding",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"std dev before and after: \", std(data),\"\\t\" ,std(offₛ))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "talented-error",
   "metadata": {},
   "source": [
    "Those values look the same, but we probably wouldn’t notice if they were different in the sixth decimal place. Let’s do this instead:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "copyrighted-outline",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"difference in standard deviations before and after: \",\n",
    "      std(data) - std(offₛ))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "central-owner",
   "metadata": {},
   "source": [
    "Again, the difference is very small. It’s still possible that our function is wrong, but it seems unlikely enough that we should probably get back to doing our analysis. We have one more task first, though: we should write some documentation for our function to remind ourselves later what it’s for and how to use it.\n",
    "\n",
    "The usual way to put documentation in software is to add comments like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "retained-accordance",
   "metadata": {},
   "outputs": [],
   "source": [
    "# offset_mean(data, target_mean_value):\n",
    "#  return a new array containing the original data with its mean offset to \n",
    "#  match the desired value.\n",
    "function offset_mean(data, target_mean_value)\n",
    "    return (data - mean(data)) + target_mean_value\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "surrounded-cologne",
   "metadata": {},
   "source": [
    "There’s a better way, though. If the first thing in a function is a string that isn’t assigned to a variable, that string is attached to the function as its documentation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "published-inspection",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"    \n",
    "    Return a new array containing the original data\n",
    "       with its mean offset to match the desired value.\n",
    "\n",
    "    Examples\n",
    "    --------\n",
    "    >>> offset_mean([1, 2, 3], 0)\n",
    "    array([-1.,  0.,  1.])\n",
    "\n",
    "\"\"\"\n",
    "function offset_mean(data, target_mean_value)\n",
    "    return (data - numpy.mean(data)) + target_mean_value \n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "mounted-helen",
   "metadata": {},
   "outputs": [],
   "source": [
    "?offset_mean"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "valuable-elizabeth",
   "metadata": {},
   "source": [
    "### Defining Defaults\n",
    "---\n",
    "The idea here is that there are default parameters that we may want to change. But if we choose not to make those changes, then the defaults will be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "legal-parade",
   "metadata": {},
   "outputs": [],
   "source": [
    "function display(a=1, b=2, c=3)\n",
    "    print(\"a: \", a,\"\\t\", \"b: \", b,\"\\t\", \"c: \", c)\n",
    "end\n",
    "\n",
    "println(\"no parameters: \")\n",
    "display()\n",
    "println()\n",
    "println(\"one parameter: \")\n",
    "display(55)\n",
    "println()\n",
    "println(\"two parameters: \")\n",
    "display(55, 66)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "third-alliance",
   "metadata": {},
   "source": [
    "### Anonymous Functions\n",
    "---\n",
    "An anonymous function is created without being given a name. As an example:\n",
    "\n",
    "```julia\n",
    "x -> x^2 + 2x -1\n",
    "```\n",
    "\n",
    "These type of functions are primarily passed to functions that take other functions as arguments, such as below, where we want to `map` a function onto an array of input values, returning values transformed by the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "stainless-horror",
   "metadata": {},
   "outputs": [],
   "source": [
    " map(x -> x^2 + 2x - 1, [1, 3, -1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fabulous-tracker",
   "metadata": {},
   "source": [
    "You can apply this to multiple inputs by using:\n",
    "\n",
    "```julia\n",
    "(x,y,z) -> x^2 + 2y - z\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "charged-check",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-danger\">\n",
    "\n",
    "\n",
    "### 📌 Key Points\n",
    "---\n",
    "- Define a function using `function function_name(parameter)`.\n",
    "- Call a function using function_name(value).\n",
    "- Variables defined within a function can only be seen and used within the body of the function.\n",
    "- If a variable is not defined within the function it is used, Julia looks for a definition before the function call\n",
    "- Use help(thing) to view help for something.\n",
    "- Put docstrings in functions to provide help for that function.\n",
    "- Specify default values for parameters when defining a function using name=value in the parameter list."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.6.0",
   "language": "julia",
   "name": "julia-1.6"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
