{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "occupational-reviewer",
   "metadata": {},
   "source": [
    "# Storing Multiple Values in Array\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "orange-singles",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "## ❓Overview\n",
    "\n",
    "**Questions**\n",
    "- How can I store many values together?\n",
    "\n",
    "**Objectives**\n",
    "- Explain what an array is.\n",
    "- Create and index of simple values.\n",
    "- Change the values of individual elements\n",
    "- Append values to an existing array\n",
    "- Reorder and slice array elements\n",
    "- Create and manipulate nested arrays"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "demographic-words",
   "metadata": {},
   "source": [
    "Similar to a string that can contain many characters, an array is a container that can store many values. We create an array by putting values inside square brackets and separating the values with commas or spaces:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "federal-speech",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds = [1, 3, 4, 7]\n",
    "print(\"odds are: \", odds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "loaded-volunteer",
   "metadata": {},
   "outputs": [],
   "source": [
    "typeof(odds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "circular-spyware",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds₁ = [1 3 4 7]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dramatic-provincial",
   "metadata": {},
   "outputs": [],
   "source": [
    "test = [\"dog\", 1, 1.5, 'a', 'δ']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "stuck-individual",
   "metadata": {},
   "source": [
    "We can access elements of an array by using indices – numbered positions of elements in the list starting at 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ecological-science",
   "metadata": {},
   "outputs": [],
   "source": [
    "println(\"first element: \", odds[1])\n",
    "println(\"last element: \", odds[4])\n",
    "println(\"end element: \", odds[end])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "divided-checkout",
   "metadata": {},
   "outputs": [],
   "source": [
    "println(\"end element: \", odds[end-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "consecutive-advisory",
   "metadata": {},
   "source": [
    "If we loop over an array, the loop variable is assigned to its elements one at a time:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bridal-junior",
   "metadata": {},
   "outputs": [],
   "source": [
    "for number in odds\n",
    "    println(number)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sustained-think",
   "metadata": {},
   "source": [
    "There is one important difference between arrays and strings: we can change the values in an array, but we cannot change individual characters in a string. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "posted-fever",
   "metadata": {},
   "outputs": [],
   "source": [
    "names = [\"Curie\", \"Darwing\", \"Turing\"]  # typo in Darwin's name\n",
    "println(\"names is originally: \", names)\n",
    "names[2] = \"Darwin\"  # correct the name\n",
    "println(\"final value of names: \", names)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "banner-brain",
   "metadata": {},
   "outputs": [],
   "source": [
    "name = \"Darwin\"\n",
    "name[1] = 'd'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "virgin-radical",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "### Ch-Ch-Ch-Ch-Changes\n",
    "---\n",
    "Data which can be modified in place is called mutable, while data which cannot be modified is called immutable. Strings and numbers are immutable. This does not mean that variables with string or number values are constants, but when we want to change the value of a string or number variable, we can only replace the old value with a completely new value.\n",
    "\n",
    "Arrays, on the other hand, are mutable: we can modify them after they have been created. We can change individual elements, append new elements, or reorder the whole list. For some operations, like sorting, we can choose whether to use a function that modifies the data in-place or a function that returns a modified copy and leaves the original unchanged.\n",
    "\n",
    "Be careful when modifying data in-place. If two variables refer to the same array, and you modify the array value, it will change for both variables!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "frequent-negotiation",
   "metadata": {},
   "outputs": [],
   "source": [
    "salsa = [\"peppers\", \"onions\", \"cilantro\", \"tomatoes\"]\n",
    "my_salsa = salsa  # <-- my_salsa and salsa point to the *same* \n",
    "                  # array data in memory\n",
    "salsa[1] = \"hot peppers\"\n",
    "print(\"Ingredients in my salsa: \", my_salsa)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "editorial-contest",
   "metadata": {},
   "source": [
    "If you want variables with mutable values to be independent, you must make a copy of the value when you assign it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "suited-findings",
   "metadata": {},
   "outputs": [],
   "source": [
    "salsa = [\"peppers\", \"onions\", \"cilantro\", \"tomatoes\"]\n",
    "my_salsa = deepcopy(salsa)  # <-- <-- makes a *copy* of the list \n",
    "                  \n",
    "salsa[1] = \"hot peppers\"\n",
    "print(\"Ingredients in my salsa: \", my_salsa)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "expected-stranger",
   "metadata": {},
   "source": [
    "Because of pitfalls like this, code which modifies data in place can be more difficult to understand. However, it is often far more efficient to modify a large data structure in place than to create a modified copy for every small change. You should consider both of these aspects when writing your code.\n",
    "\n",
    "### Nested Lists\n",
    "---\n",
    "Since an array can contain any kind of variable, it can even contain other arrays.\n",
    "For example, we could represent the products in the shelves of a small grocery shop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "simplified-letter",
   "metadata": {},
   "outputs": [],
   "source": [
    "x = [[\"pepper\", \"zucchini\", \"onion\"],\n",
    "     [\"cabbage\", \"lettuce\", \"garlic\"],\n",
    "     [\"apple\", \"pear\", \"banana\"]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "elegant-narrative",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "defensive-trainer",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(x[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "great-broadcast",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Print using the value of x that returns the item pepper below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "basic-defense",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "right-petersburg",
   "metadata": {},
   "source": [
    "### Manipulating Lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recorded-match",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds = [1, 3, 5, 7]\n",
    "new_items = [2, 4, 8, 10]\n",
    "append!(odds,new_items)\n",
    "odds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bizarre-orientation",
   "metadata": {},
   "outputs": [],
   "source": [
    "pop!(odds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "foreign-onion",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "closing-batch",
   "metadata": {},
   "outputs": [],
   "source": [
    "deleteat!(odds, 4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "durable-toronto",
   "metadata": {},
   "outputs": [],
   "source": [
    "push!(odds, 57)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "agricultural-flavor",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds[1] = 55"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "subjective-apparatus",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "nonprofit-roller",
   "metadata": {},
   "outputs": [],
   "source": [
    "splice!(odds, 3, 2) # remove the item at index 3 with 2, and return 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "original-porcelain",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "attractive-horror",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Display the reverse of values in odds below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "tamil-canada",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "opponent-conservative",
   "metadata": {},
   "source": [
    "While modifying in place, it is useful to remember that Julia treats arrays in a slightly counter-intuitive way.\n",
    "\n",
    "As we saw earlier, when we modified the salsa array item in-place, if we make an array, (attempt to) copy it and then modify this array, we can cause all sorts of trouble. This also applies to modifying the list using the above functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "complete-focus",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds = [1, 3, 5, 7]\n",
    "primes = odds\n",
    "append!(primes, 2)\n",
    "println(\"primes:\", primes)\n",
    "println(\"odds:\", odds)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "sapphire-robin",
   "metadata": {},
   "source": [
    "This is because Julia stores an array in memory, and then can use multiple names to refer to the same array. If all we want to do is copy a (simple) array, we can again use the `deepcopy` function, so we do not modify a list we did not mean to:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acting-force",
   "metadata": {},
   "outputs": [],
   "source": [
    "odds = [1, 3, 5, 7]\n",
    "primes = deepcopy(odds)\n",
    "append!(primes, 2)\n",
    "println(\"primes: \", primes)\n",
    "println(\"odds: \", odds)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "innocent-basis",
   "metadata": {},
   "source": [
    "### Turn a String into a List\n",
    "---\n",
    "Use a for-loop to convert the string \"hello\" into an array of letters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "attached-radar",
   "metadata": {},
   "outputs": [],
   "source": [
    "my_list = []\n",
    "\n",
    "for char in \"hello\"\n",
    "    append!(my_list, char)\n",
    "end\n",
    "\n",
    "print(my_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "quarterly-mapping",
   "metadata": {},
   "source": [
    "Subsets of arrays and strings can be accessed by specifying ranges of values in brackets. This is commonly referred to as “slicing.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "optical-precipitation",
   "metadata": {},
   "outputs": [],
   "source": [
    "binomial_name = \"Drosophila melanogaster\"\n",
    "group = binomial_name[1:11]\n",
    "println(\"group: \", group)\n",
    "\n",
    "species = binomial_name[12:23]\n",
    "println(\"species: \", species)\n",
    "\n",
    "chromosomes = ['X', 'Y', '2', '3', '4']\n",
    "autosomes = chromosomes[3:5]\n",
    "println(\"autosomes: \" , autosomes)\n",
    "\n",
    "last = chromosomes[end]\n",
    "println(\"last: \", last)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "presidential-deposit",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*How can you determine how long the string \"Drosophila melanogaster\" is?*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "necessary-archives",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "hindu-blanket",
   "metadata": {},
   "source": [
    "### Slicing from the End\n",
    "---\n",
    "Use slicing to access only the last four characters of a string or entries of a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "southwest-aruba",
   "metadata": {},
   "outputs": [],
   "source": [
    "string_for_slicing = \"Observation date: 02-Feb-2013\"\n",
    "list_for_slicing = [[\"fluorine\", \"F\"],\n",
    "                    [\"chlorine\", \"Cl\"],\n",
    "                    [\"bromine\", \"Br\"],\n",
    "                    [\"iodine\", \"I\"],\n",
    "                    [\"astatine\", \"At\"]]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "continuous-perth",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Print out the values for all but the first pair in list_for_slicing below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "recognized-alabama",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "external-specification",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "\n",
    "*Print out the values for only the year in string_for_slicing below:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "afraid-title",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "quick-commercial",
   "metadata": {},
   "source": [
    "Would your solution work regardless of whether you knew beforehand the length of the string or array (e.g. if you wanted to apply the solution to a set of arrays of different lengths)? If not, try to change your approach to make it more robust."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cognitive-welsh",
   "metadata": {},
   "source": [
    "### Non-Continuous Slices\n",
    "---\n",
    "So far we’ve seen how to use slicing to take single blocks of successive entries from a sequence. But what if we want to take a subset of entries that aren’t next to each other in the sequence?\n",
    "You can achieve this by providing a third argument to the range within the brackets, called the step size. The example below shows how you can take every third entry in an array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "herbal-decimal",
   "metadata": {},
   "outputs": [],
   "source": [
    "primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]\n",
    "subset = primes[1:3:length(primes)]\n",
    "print(\"subset \", subset)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "liked-windows",
   "metadata": {},
   "source": [
    "Notice that the slice taken begins with the first entry in the range, followed by entries taken at equally-spaced intervals (the steps) thereafter. If you wanted to begin the subset with the third entry, you would need to specify that as the starting point of the sliced range:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "smoking-extension",
   "metadata": {},
   "outputs": [],
   "source": [
    "primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]\n",
    "subset = primes[3:3:12]\n",
    "print(\"subset\", subset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "upset-lecture",
   "metadata": {},
   "outputs": [],
   "source": [
    "date = \"Monday 4 January 2016\"\n",
    "day = date[1:6]\n",
    "println(\"Using 1 to begin range: \", day)\n",
    "day = date[begin:6]\n",
    "println(\"Using begin: \", day)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "likely-grade",
   "metadata": {},
   "outputs": [],
   "source": [
    "months = [\"jan\", \"feb\", \"mar\", \"apr\", \"may\", \"jun\", \"jul\", \"aug\", \"sep\", \"oct\", \"nov\", \"dec\"]\n",
    "sond = months[8:12]\n",
    "println(\"With known last position: \", sond)\n",
    "sond = months[8:length(months)]\n",
    "println(\"Using length() to get last entry: \", sond)\n",
    "sond = months[8:end]\n",
    "print(\"Using end: \", sond)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "moderate-underground",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-danger\">\n",
    "\n",
    "### 📌 Key Points\n",
    "---\n",
    "- `[value1, value2, value3, ...]` creates an array.\n",
    "- Arrays can contain any object, including arrays (i.e., array of arrays).\n",
    "- Arrays are indexed and sliced with square brackets (e.g., `array[0]` and `array[2:9]`), in the same way as strings.\n",
    "- Arrayss are mutable (i.e., their values can be changed in place).\n",
    "- Strings are immutable (i.e., the characters in them cannot be changed)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.4",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
