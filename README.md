# Programming with Julia

## Julia Pros and Cons

### Pros
1. Speed - Compiled language using JIT compiler
2. Syntactically similar to interpreted languages like R & Python
3. Can be run in R and Python - `JuliaCall` and `PyJulia`
4. Multiple Dispatch

### Cons
1. Smaller user base
2. Fewer Packages - but `PyCall` and `RCall` works well
3. Time to Plot - but `v1.6`

## Apolipoprotein E (ApoE) - from [Wikipedia](https://en.wikipedia.org/wiki/Apolipoprotein_E) 

Apolipoprotein E (APOE) is a protein involved in the metabolism of fats in the body of mammals. A subtype is implicated in Alzheimer's disease. APOE is the principal cholesterol carrier in the brain. The E4 variant was the largest known genetic risk factor for late-onset sporadic Alzheimer's disease (AD) in a variety of ethnic groups. Although 40–65% of AD patients have at least one copy of the E4 allele, APOE4 is not a determinant of the disease. 

At least one third of patients with AD are APOE4 negative and some APOE4 homozygotes never develop the disease. Yet those with two ε4 alleles have up to 20 times the risk of developing AD.There is also evidence that the APOE2 allele may serve a protective role in AD.Thus, the genotype most at risk for Alzheimer's disease and at an earlier age is APOE4,4. Using genotype APOE3,3 as a benchmark (with the persons who have this genotype regarded as having a risk level of 1.0), individuals with genotype APOE4,4 have an odds ratio of 14.9 of developing Alzheimer's disease. Individuals with the APOE3,4 genotype face an odds ratio of 3.2, and people with a copy of the 2 allele and the 4 allele (APOE2,4), have an odds ratio of 2.6. Persons with one copy each of the 2 allele and the 3 allele (APOE2,3) have an odds ratio of 0.6. Persons with two copies of the 2 allele (APOE2,2) also have an odds ratio of 0.6.

We have experimental data about how fragments from 2 ApoE variants (ApoE3 and ApoE3) differentially affect gene regulation. We would like to assess this data to learn more about the variants differences and similarities. The data, contained in a list of comma seperated values, shows the number of times greater that a gene was produced than when compared to a control sample. We call this fold change - so a 2 fold change has 2 times the product as the control. 

<div align="center">

![CPEB3 Ribozyme](Images/apoe.png)

</div>

## Schedule

| Time | Topic(s) | Description |
| :-: | --- | --- |
| | [Setup](Presentation/setup.md) |  Setup Jupyter notebook |
| 00:00 | [Julia Fundamentals](/Code/Fundamentals.ipynb) |  |
| 00:30 | [Analyzing APOE Data](/Code/Analyze.ipynb) |  |
| 01:30 | [Visualizing Tabular Data](/Code/Visualize.ipynb) |  |
| 02:20 | [Repeating Actions with Loops](//Code/Loop.ipynb) |  |
| 02:50 | [Storing Multiple Values](/Code/Store.ipynb) |  |
| 03:35 | [Creating Functions](/Code/Function.ipynb) |  |
| 04:00 | Wrap-up |  |





