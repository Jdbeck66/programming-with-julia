[Return Home](../README.md)

# Use Julia and Jupyter Notebooks

Use a the prebuilt Jupyter notebook available at [Codelab](https://codelab.boisestate.edu)


# Or, install Julia and Jupyter notebooks on you machine by using the following instructions
## Install Julia 1.5.4
---

Download Julia 1.5.4 from [https://julialang.org/downloads](https://julialang.org/downloads) for your operating system (Linux x86, Mac, Windows, etc).

![Download Selections](../Images/Julia_Download.png)

Click on the file you downloaded to install Julia on your computer.

## Julia REPL in Terminal
---

We can test that Julia has been installed correctly by running it in a terminal. Open up a terminal window and type `Julia`.  At the Julia prompt, enter `1+1` and see what happens. 

You should see something like this:

![Julia REPL](../Images/Julia_REPL.png)

>>>
If you use the Windows operating system, you may need to to download and install additional software to access a terminal. You can try [MobaXTerm](https://mobaxterm.mobatek.net/download.html) or [Git Bash](https://gitforwindows.org) within the `Git for Windows` download.
>>>

## Add Julia to Jupyter Notebook
---

We will next install Jupyter notebooks for Julia. To do so, we will move to the Julia package manager by entering `]` (a closing square backet) at the julia prompt. This will open Julia's package manager and it will look like this:

```julia
julia> ]

(@v1.5) pkg>

```

Install Jupyter notebooks package for Julia by typing `add IJulia` at the prompt. It will look like this:

```julia
(@v1.5) pkg> add IJulia
```

The installation process may take a couple of minutes. Once the installation is complete and you have been returned to the `pkg` prompt, you can exit the package manager by entering `Control-c` and then closing your terminal window.

## Download and install Anaconda to access Jupyter notebooks
---
Download and install the Anaconda Distribution for your operating system found [here](https://www.anaconda.com/products/individual).

## Class Content
---


[Return Home](../README.md)
